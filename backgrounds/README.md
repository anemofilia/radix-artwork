This repo is a fork of the [Guix Artwork](https://git.savannah.gnu.org/cgit/guix/guix-artwork.git/) repository.

Copying
-------

For any SVG file in this directory, see the correspondent metadata for
information about authors and copyright. If you are using Inkscape, you can
access the Document metadata from the File menu.
